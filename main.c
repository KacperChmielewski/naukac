#include <stdio.h>



struct produkt{
    char* nazwa;
    double cena;
    struct produkt* next;
};
typedef struct produkt produkt;

void produkt_init(produkt* p, char* nazwa, double cena){
	p->nazwa = nazwa;
	p->cena = cena;
	p->next = NULL;
}

void produkt_display(produkt* p){
	printf("%s koszt %f\r\n", p->nazwa, p->cena);
}



struct produkt_head{
    struct produkt* first;
    struct produkt* last;
};
typedef struct produkt_head produkt_head;

void produkt_head_init(produkt_head* h){
	h->first = NULL;
	h->last = NULL;
}

void produkt_list_add(produkt_head* h, produkt* prod){
	// sprawdzamy czy lista zawiera jakiekolwiek elementy. Domyslnie oba wskazniki w "glowie" listy maja wartosc NULL
	if(!h->first){	// jesli lista nie zostala zainicjalizowana
		h->first = prod;
		h->last = prod;
	} else{
		h->last->next = prod;		// ustawiamy w ostatnim elemencie dotychczasowego koszyka wskaznik na nowy element
		h->last = prod;				// ustawiamy nowododany element jako ostatni
	}
}

void produkt_list_display(produkt_head* h){
	produkt* tymczasowyWskaznikNaProdukt;
	
	printf("--- ZAWARTOSC KOSZYKA ---\r\n");
	
	tymczasowyWskaznikNaProdukt = h->first;
	while(tymczasowyWskaznikNaProdukt){
		produkt_display(tymczasowyWskaznikNaProdukt);
		tymczasowyWskaznikNaProdukt = tymczasowyWskaznikNaProdukt->next;
	}
}


int main(){
	produkt mydlo;
	produkt_init(&mydlo, "Mydelko", 1.59);
	
	produkt bulka;
	produkt_init(&bulka, "buleczka", 0.49);

	produkt maslo;
	produkt_init(&maslo, "maselko", 9.99);
	
	
	produkt_head koszyk;
	
	produkt_head_init(&koszyk);
	
	produkt_list_add(&koszyk, &mydlo);
	produkt_list_add(&koszyk, &bulka);
	produkt_list_add(&koszyk, &maslo);


	produkt_list_display(&koszyk);
	
	

return 1;

}

